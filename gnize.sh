
#Gnize: An interactive script to manage Flexget and My Anime List.
#Copyright (C) 2013  Adam Robert Mumford amum4d@gmail.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash
clear
logoo()
{
nub=1
#echo " ___________________________________"
echo "  ______ __   _ _____ ______ _______"
echo " |  ____ | \  |   |    ____/ |______"
echo " |_____| |  \_| __|__ /_____ |______"
#echo " ___________________________________"
echo ""
}
logo()
{
echo
nub=1
}
################################
################################
# half this stuff doesnt even work
if [ "$1" = "-h" ] 
then
echo Usage:
echo [-l] Show list
echo [-u] Show updates
echo [-a name] Add to list
echo [-r name] Remove from list
echo [-fu] More detailed update
echo [-run NAME] Run flexget task
echo [-reload] unload and reload flex get launch agent
echo

exit
fi

if [ "$1" = "-reload" ]
then
	launchctl unload /Users/$USER/Library/LaunchAgents/com.flexget.plist
	launchctl load /Users/$USER/Library/LaunchAgents/com.flexget.plist
fi

if [ -f "/Volumes/Mini/.flexget/config.yml" ]
then
dataFile="/Volumes/Mini/.flexget/config.yml" #path to config?
else
echo "Can't find /Volumes/Mini/.flexget/config.yml"
sleep 2
exit
fi

if [ ! -d "$HOME/.gnize" ]
then
mkdir "$HOME/.gnize"
fi
home="$HOME/.gnize/"

startToken="token 1"
stopToken="token 2"

startToken3="token 3"
stopToken4="token 4"

show=$2
name="      - $show"
show2=$3
name2="      - $show2"

break='\
'

if [ "$1" = "-run" ]
then
	flexget --task $2
fi

###################
############## ADD

if [ "$1" = "-a" ]
then
	if [ "$2" = "" ]
	then
	echo "Nothing added"
	exit
	fi
sed -i "" "s#\#token 2#$name$break\\#token 2#" ${dataFile}
fi

#####################
############## REMOVE

if [ "$1" = "-r" ]
then
	if [ "$2" = "" ]
	then
	echo "Nothing removed"
	exit
	fi
#sed -i "/$name/d" ${dataFile}
sed -i -e "/$name/d" ${dataFile}
fi

#############################
############## LINE DETECTION

startTokenLine=$( grep -n "${startToken}" "${dataFile}" | cut -f 1 -d':' )
stopTokenLine=$( grep -n "${stopToken}" "${dataFile}" | cut -f 1 -d':' )

let stopTokenLine=stopTokenLine-1
let tailLines=stopTokenLine-startTokenLine

list()
{
#echo "LIST:"
echo -e "[U]PDATES | \033[31m[L]IST\033[39m | [Q]UIT"
echo
head -n ${stopTokenLine} ${dataFile} | tail -n ${tailLines} | cut -c6-300
echo
echo -n \>
read -n2 keypress
}

if [ "$*" = "-l" ]
then
list
fi

############################
############## FILE DETECTION

checknew()
{
if grep -Fxq $1 $HOME/.gnize/.gnizex.mastermem
then
echo $1

else
echo $1 >> $HOME/.gnize/.gnizex.mastermem
echo -n $F
echo -e "$file\033[31m NEW!\033[39m"
fi
}

update()
{
#tput civis
#echo UPDATES:
echo -e "\033[31m[U]PDATES\033[39m | [L]IST | [Q]UIT"
echo

dir="/Volumes/Cicada/Plex/Watching/" #where your shows are stored


if [ ! -f $HOME/.gnize/.gnizex.mastermem ]
		then
			touch $HOME/.gnize/.gnizex.mastermem
		fi	

if [ ! -f "$HOME/.gnize/gnizex.txt" ]
	then
		touch $HOME/.gnize/gnizex.txt
fi	

old_IFS=IFS
IFS=$'\n'
entries=($(head -n ${stopTokenLine} $dataFile | tail -n $tailLines | cut -c9-300))
#IFS=old_IFS

COUNTER=0
while [  $COUNTER -lt ${#entries[@]} ]; do


	show=${entries[$COUNTER]}
	pth1=$(( ${#dir} + ${#show} + 2))
	mem="$HOME/.gnize/${entries[$COUNTER]}.mem"
	status=$(find "$dir${entries[$COUNTER]}" -name '*.mkv' | sort -t '-' -k 2n | tail -1 | grep -E -o '[0-9]{2}[ _.v]' | cut -c1-2 )
#find "$dir${entries[$COUNTER]}" -name '*.mkv' | sort #'-' -k 2n 
	if [ ! -d "$dir${entries[$COUNTER]}" ] 		#create folder
		then
			mkdir "$dir$show"
	fi

	if [ ! -f $HOME/.gnize/${entries[$COUNTER]}.mem ]		#create file
		then
			touch $HOME/.gnize/${entries[$COUNTER]}.mem
			echo "watched=$status" > "$HOME/.gnize/${entries[$COUNTER]}.mem"
			source "$mem"
		else
			source "$mem"

			if [ "$watched" == "" -o "$watched" == " " ]
			then
			watched=0
			fi	
	fi

	#let next=watched+1 #delete these lines
	#find "$dir${entries[$COUNTER]}" -name '*.mkv' | grep -E "$next[ ]" #regex nees be better

		if [ "$status" == "" -o "$status" == " " ] 
		then
			status=0
			#echo "$show Nothing found"
		fi

#count=`expr ${status} - ${watched}` #delete this too
status=`echo $status|sed 's/^0*//'`
count=$(($status-$watched))

let firunwatched=watched+1
#echo watched: $watched
#echo firunwatched: $firunwatched

#echo
#echo $show
#echo $status
#echo $watched #Need to build in more diognostics / options


	if [[ "$count" > "0" ]]		#if there are unwatched epps
	then

		echo -ne "\033[31m[\033[39m" 
		echo -n $nub
		echo -ne "\033[31m]\033[39m " #print pretty number
tput smul
		echo $show
tput rmul

fle[$nub]=$(find "$dir""$show" -name '*.mkv' | sort -t '-' -k 2n | tail -$count | head -1)
shw[$nub]=$(find "$dir""$show" -name '*.mkv' | sort -t '-' -k 2n | tail -$count | head -1 | cut -c$pth1-300)
wtc[$nub]=$( echo $watched )
nme[$nub]=$( echo $show )

#echo full filename ${fle[$nub]}
#echo shortname ${shw[$nub]}
#echo watched ${wtc[$nub]}
#echo showname ${nme[$nub]}




		
for F in $(find "$dir""$show" -name '*.mkv' | sort -t '-' -k 2n | tail -$count | head -1 | cut -c$pth1-300) #for first unwatched of each show
do

	checknew $F	


#let nub=nub+1 #increment tags per episode. not very usefull.
done

for g in $(find "$dir""$show" -name '*.mkv' | sort -t '-' -k 2n | tail -$count | tail -n +2 | cut -c$pth1-300)
do

#echo -n "   "
checknew $g

done
echo

	let nub=nub+1 #increment tags per show #breaks gnizex.txt

	fi

let COUNTER=COUNTER+1

done
launch
menu
}

############################
############## FILE LAUNCHER
vid="/Users/adam/Library/Application Support/MPlayer OSX Extended/Binaries/mplayer2.mpBinaries/Contents/MacOS/"
#ar=“-fs --xineramascreen=1 --really-quiet"

#mpv -vf screenshot -screen 1 -fs --really-quiet "$L1"
# >mplayer lol nope
eo="-vf screenshot -xineramascreen 1 -fs --really-quiet" #mpv args
##############

launch()
{
#tput civis
tput sc
echo -n \>
read -n2 keypress
tput rc
#source $HOME/.gnize/gnizex.txt #loading episodes just defined above from gnizex.txt
#2> error.log

if [ "$keypress" = "l" ]
then
#sleep
tput rc
#tput ed
##tput cup 0 0
tput clear
logo
list
fi

if [ "$keypress" = "u" ]
then
tput rc
tput clear
#tput cup 0 0
#tput clear
logo
update
#launch
menu
fi

if [ "$keypress" = "q" ]
then
#sleep
tput rc
tput clear
##tput cup 0 0
#tput clear
tput cnorm
exit
fi

#if [ "$keypress" = "x" ]
#then
#echo "Playing all: Now playing $F1"
##"$vid" -xineramascreen 1 -fs --really-quiet "$L1" 2> error.log
#W1=$(($W1 + 1))
##echo "watched=$W1" > "$S1.gnize.mem"
#clear
#logo
#echo x | update
#fi


	tput smso
	echo "Now playings ${shw[$keypress]}"
	tput rmso
	mpv -screen 1 -fs --really-quiet  ${fle[$keypress]}
	add=${wtc[$keypress]}
	let add=add+1
	echo "watched=$add" > "$home${nme[$keypress]}.mem"
	tput rc
	tput clear
	logo
	update


}

################################## MENU
tput sc
logo
echo "[U]PDATES | [L]IST | [Q]UIT"

menu()
{
#tput sc
#tput civis
read -s -n1 keypress
#echo $keypress
#echo; echo "Keypress was "\"$keypress\""."

if [ "$keypress" = "u" ]
then
tput rc
#tput ed
#tput cup 0 0
tput clear
logo
update
#launch
menu
fi


if [ "$keypress" = "l" ]
then
tput rc
#tput ed
#tput cup 0 0
tput clear
logo
list
menu
fi

if [ "$keypress" = "q" ]
then
tput rc
tput clear
tput cnorm
#tput ed
clear
exit
fi
}
###################
menu
echo exiting
sleep 10
clear


